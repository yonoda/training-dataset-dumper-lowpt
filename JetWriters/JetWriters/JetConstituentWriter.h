#ifndef JET_CONSTITUENT_WRITER_H
#define JET_CONSTITUENT_WRITER_H

#include "JetConstituentWriterFwd.h"
#include "JetWriters/JetElement.h"

#include "xAODJet/JetFwd.h"
#include "JetConstituentWriterConfig.h"
#include "HDF5Utils/Writer.h"

namespace H5 {
  class Group;
}

template <typename T, typename A>
class JetConstituentWriter
{
public:
  JetConstituentWriter(H5::Group& output_group,
                       const JetConstituentWriterConfig& config,
                       A association = {});
  JetConstituentWriter(JetConstituentWriter&) = delete;
  JetConstituentWriter operator=(JetConstituentWriter&) = delete;
  JetConstituentWriter(JetConstituentWriter&&);
  void write(const xAOD::Jet& jet, const std::vector<const T*>& constituents);
  void flush();
private:
  using ElementType = JetElement<T>;
  using ConstituentWriter = H5Utils::Writer<1, const ElementType&>;
  ConstituentWriter m_writer;
};

#include "JetConstituentWriter.icc"

#endif
