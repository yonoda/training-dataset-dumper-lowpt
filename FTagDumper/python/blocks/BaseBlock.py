from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


@dataclass
class BaseBlock(ABC):
    """
    Base class for all blocks to inherit from.
    This class ensures all blocks have access to the dumper configuration and the job `flags`.
    """
    dumper_config: dict[str, Any]
    flags: Any

    @abstractmethod
    def to_ca(self) -> ComponentAccumulator:
        pass
