#include "TriggerVRJetOverlapDecoratorTool.h"
#include "FlavorTagDiscriminants/VRJetOverlapDecorator.h"

TriggerVRJetOverlapDecoratorTool::TriggerVRJetOverlapDecoratorTool(const std::string& name):
        asg::AsgTool(name),
        m_dec(nullptr)
    {
    }
TriggerVRJetOverlapDecoratorTool::~TriggerVRJetOverlapDecoratorTool() {}
    
StatusCode TriggerVRJetOverlapDecoratorTool::initialize() {
    m_dec.reset(new VRJetOverlapDecorator(VRJetOverlapConfig(VRJetParameters::RHO30MIN02MAX4)));
    return StatusCode::SUCCESS;
}

StatusCode TriggerVRJetOverlapDecoratorTool::decorate(const xAOD::JetContainer& jets) const {
    m_dec->decorate(jets);
    return StatusCode::SUCCESS;
}
