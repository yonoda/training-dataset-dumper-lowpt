#include "JetLeptonDecayLabelDecorator.hh"

#include "TruthTools.hh"

#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"

// the constructor just builds the decorator
JetLeptonDecayLabelDecorator::JetLeptonDecayLabelDecorator(const std::string& prefix):
  m_decoDecay(prefix + "LeptonDecayLabel"),
  m_decoTau(prefix + "TauDecayLabel")
{
}

// this call actually does the work on the jet
void JetLeptonDecayLabelDecorator::decorate(const xAOD::Jet& jet) const {

  //regular b-tagging :
  std::vector<const xAOD::TruthParticle*> ConeAssocBHad;
  const std::string labelB = "ConeExclBHadronsFinal";
  jet.getAssociatedObjects<xAOD::TruthParticle>(labelB, ConeAssocBHad);

  std::vector<const xAOD::TruthParticle*> ConeAssocCHad;
  const std::string labelC = "ConeExclCHadronsFinal";
  jet.getAssociatedObjects<xAOD::TruthParticle>(labelC, ConeAssocCHad); 

  // count all electrons and muons in b- and c- decay. the decay products of the
  // b hadron decay include those from the c hadron decay.
  m_decoDecay(jet) = -999;
  m_decoTau(jet) = -999;

  if (ConeAssocBHad.size() != 0 || ConeAssocCHad.size() != 0){
    auto Allleptons = countLeptons(ConeAssocBHad, jet);

    // count all electrons and muons from the c decay only.
    auto Cleptons = countLeptons(ConeAssocCHad, jet);

    // Get number of electrons/muon from b hadrons 
    int el_fromB = Allleptons.el_fromHad - Cleptons.el_fromHad;
    int mu_fromB = Allleptons.mu_fromHad - Cleptons.mu_fromHad;
    int tau_fromB = Allleptons.tau_fromHad - Cleptons.tau_fromHad;
    int tauel_fromB = Allleptons.el_fromTau - Cleptons.el_fromTau;
    int taumu_fromB = Allleptons.mu_fromTau - Cleptons.mu_fromTau;

    int el_fromC = Cleptons.el_fromHad;
    int mu_fromC = Cleptons.mu_fromHad;
    int tau_fromC = Cleptons.tau_fromHad;
    int tauel_fromC = Cleptons.el_fromTau;
    int taumu_fromC = Cleptons.mu_fromTau;

    // Get extended label for semileptonic vs hadronic b/c decays
    std::vector<int> leps_in_decays;
    if (tau_fromB > 0){ leps_in_decays.push_back(15); }
    if (tau_fromC > 0){ leps_in_decays.push_back(15); }
    if (mu_fromB > 0){ leps_in_decays.push_back(13); }
    if (mu_fromC > 0){ leps_in_decays.push_back(13); }
    if (el_fromB > 0){ leps_in_decays.push_back(11); }
    if (el_fromC > 0){ leps_in_decays.push_back(11); }

    std::vector<int> tauleps_in_decays;
    if (tauel_fromB > 0){ tauleps_in_decays.push_back(11); } else {tauleps_in_decays.push_back(0); }
    if (tauel_fromC > 0){ tauleps_in_decays.push_back(11); } else {tauleps_in_decays.push_back(0); }
    if (taumu_fromB > 0){ tauleps_in_decays.push_back(13); } else {tauleps_in_decays.push_back(0); }
    if (taumu_fromC > 0){ tauleps_in_decays.push_back(13); } else {tauleps_in_decays.push_back(0); }

    // For the LeptonDecayLabel 1 means electron, 2 muon and 3 tau. The leptons in the b- and c-hadron 
    // decay are counted. The occurrence of each lepton is only counted once per b-/c-decay. If a lepton
    // occurs in one of the decays the corresponding number associated with the lepton is added to the 
    // label. This means:
    // 0: all hadronic b- and c- decays
    // 1: electrons in either b or c decay
    // 2: muons in either b or c decay
    // 3: taus in either b or c decay
    // 11: electrons in both decays
    // 112: electrons in both decays and muons in either b or c decay
    // ...
    // 112233: electrons, muons and taus in both decays
    m_decoDecay(jet) = getDecayLabel(leps_in_decays);

    // TauDecayLabel is defined in the same manner as the LeptonDecayLabel but is only counting the electrons
    // and muons from tau decays:
    // 0: hadronically decaying tau
    // 1: electron in tau decay
    // 2: muon in tau decay
    m_decoTau(jet) = getTauLabel(tauleps_in_decays, leps_in_decays);
  }

}

LeptonCounter JetLeptonDecayLabelDecorator :: countLeptons(const std::vector<const xAOD::TruthParticle*> &Had, const xAOD::Jet &jet) const { 
  LeptonCounter lep_counter;
  if ( Had.size()>0 ) {
    std::vector<int> HadIndices = truth::getDRSortedIndices(Had, jet);
    for ( unsigned int iHad=0; iHad < Had.size(); iHad++) {
      const xAOD::TruthParticle* myHad = Had.at(HadIndices[iHad]);
      std::vector<const xAOD::TruthParticle*> truthFromHad;
      truth::getAllChildren(myHad, truthFromHad);
      for(unsigned int i=0; i< truthFromHad.size(); i++){
        const xAOD::TruthParticle* truth_particle = truthFromHad.at(i);
        if( std::abs(truth_particle->pdgId()) == 11) { lep_counter.el_fromHad += 1; }
        if( std::abs(truth_particle->pdgId()) == 13) { lep_counter.mu_fromHad += 1; }
        if( std::abs(truth_particle->pdgId()) == 15) { 
          lep_counter.tau_fromHad += 1;
          if (truthFromHad.size() > i+3) {
            for (unsigned int j=1; j<=3; j++){
              const xAOD::TruthParticle* truth_particle_from_tau = truthFromHad.at(i+j);
              if (std::abs(truth_particle_from_tau->pdgId()) == 11) { lep_counter.el_fromTau += 1;}
              if (std::abs(truth_particle_from_tau->pdgId()) == 13) { lep_counter.mu_fromTau += 1;}
            }
          }
        }
      }  
    }
  }
  // return a tuple with the number of electrons, muons and taus from the b- or c-hadron and the leptons 
  // from the tau decay
  return lep_counter;
}

int JetLeptonDecayLabelDecorator :: getDecayLabel(const std::vector <int> leps_in_decays) const {
  int factor = 1;
  int decay_label = 0;
  for ( unsigned int iLep=0; iLep < leps_in_decays.size(); iLep++) {
    if (leps_in_decays.at(iLep) == 15) { decay_label += factor*3; }
    if (leps_in_decays.at(iLep) == 13) { decay_label += factor*2; }
    if (leps_in_decays.at(iLep) == 11) { decay_label += factor*1; }
    factor *= 10;
  }
  return decay_label;
}

int JetLeptonDecayLabelDecorator :: getTauLabel(const std::vector <int> tauleps_in_decays, const std::vector <int> leps_in_decays) const {
  int factor = 1;
  int taudecay_label = -999;
  if (std::find(leps_in_decays.begin(), leps_in_decays.end(), 15) != leps_in_decays.end()){
    taudecay_label = 0;
    for ( unsigned int iLep=0; iLep < tauleps_in_decays.size(); iLep++) {
      if (tauleps_in_decays.at(iLep) == 11) { 
        taudecay_label += factor; 
        factor *= 10;
      }
      if (tauleps_in_decays.at(iLep) == 13) { 
        taudecay_label += 2*factor; 
        factor *= 10;
      }
    }
  }
  return taudecay_label;
}